import './App.css';
import AddTodo from "./components/AddTodo/AddTodo";
import List from "./components/List/List";

function App() {
  return (
    <div className="App">
        <div className="top"> <h2>Todo List</h2></div>
        <AddTodo/>
        <List/>
    </div>
  );
}

export default App;
