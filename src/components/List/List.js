import React from 'react';
import {connect} from "react-redux";
import {removeTodo} from "../../store/actions/actions";
import './List.css';

const mapState = (state) => ({
    todos: state.todos.data,
});

const List = (props) => {

    return (
        <ul>
            {props.todos.map((todo) =>
                <li key={todo.id}>{todo.message}
                    <button className="btn" onClick={() => props.dispatch(removeTodo(todo.id))}>Delete</button>
                </li>
            )}
        </ul>
    );
};

export default connect(mapState)(List);