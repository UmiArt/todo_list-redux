import {createStore} from "redux";
import rootReducer from "./reduser/todos";

const store= createStore(rootReducer);

export default store;